﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;
using UIKit;

namespace TestProject.iOS.ViewControllers
{
    public class BaseViewController<TViewModel> : MvxViewController<TViewModel> where TViewModel : class, IMvxViewModel
    {
        public BaseViewController(string nibName, NSBundle bundle) : base(nibName, bundle) { Initialize(); }

        public BaseViewController(IntPtr handle) : base(handle) { Initialize(); }

        public BaseViewController() { Initialize(); }

        protected UITapGestureRecognizer endEdition;

        void Initialize()
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        protected virtual void UpdateNavigationBar()
        {
            if (NavigationController != null)
            {
                NavigationController.NavigationBar.Translucent = false;
                var rightBell = new UIButton();
                rightBell.SetImage(UIImage.FromBundle("ic_bell"), UIControlState.Normal);
                var badge = new UIButton();
                badge.SetImage(UIImage.FromBundle("ic_badge"), UIControlState.Normal);
                badge.ContentEdgeInsets = new UIEdgeInsets(-5,30,15,-17);
                rightBell.ContentEdgeInsets = new UIEdgeInsets(1,1,1,1);
                rightBell.AddSubview(badge);
                var rightItem = new UIBarButtonItem(rightBell);
                NavigationItem.RightBarButtonItem = rightItem;

                var leftView = new UIImageView();
                leftView.Image = UIImage.FromBundle("ic_adventure");
                leftView.ContentMode = UIViewContentMode.ScaleAspectFit;
                var leftItem = new UIBarButtonItem(leftView);
                NavigationItem.LeftBarButtonItem = leftItem;

                var title = new UILabel();
                title.Text = "Electric Adventure";
                title.TextColor = UIColor.White;
                NavigationItem.TitleView = title;

                NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(232, 31, 138);
            }

        }
    }
}