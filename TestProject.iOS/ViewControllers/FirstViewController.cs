﻿using Core.ViewModels;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views.Presenters.Attributes;
using System.Linq;
using TestProject.iOS.Layouts;
using TestProject.iOS.Sources;
using TestProject.iOS.Views;
using UIKit;

namespace TestProject.iOS.ViewControllers
{
    [MvxTabPresentation(WrapInNavigationController = true, TabName = "First", TabIconName = "CollectionImage")]
    public partial class FirstViewController : BaseViewController<FirstViewModel>
    {
        #region Fields
        private CustomCollectionViewSource collectionViewSource;
        private CustomTableViewSource tableViewSource;
        private CustomTopView topView;
        private CustomTopCollectionViewSource topCollectionViewSource;
        private NSLayoutConstraint TopCollectionView_TopConstraint { get; set; }
        #endregion

        public FirstViewController() : base("FirstViewController", null)
        {
        }

        #region Overrides
        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            collectionViewSource = new CustomCollectionViewSource(collectionView);
            collectionView.Source = collectionViewSource;          

            tableViewSource = new CustomTableViewSource(tableView);
            tableView.Source = tableViewSource;

            topView = CustomTopView.Create();
            topView.Hidden = true;
            View.AddSubview(topView);
            topCollectionViewSource = new CustomTopCollectionViewSource(topView.TopCollectionView);
            topView.TopCollectionView.Source = topCollectionViewSource;

            AddConstraints();
            AddBindings();
            AddSwipeGestureRecognizers();
        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            topView.Frame = new CoreGraphics.CGRect(topView.Frame.X, topView.Frame.Y, UIScreen.MainScreen.Bounds.Width, topView.Frame.Height);
            collectionView.CollectionViewLayout = new CustomCollectionViewLayout(collectionView.Frame);
            topView.TopCollectionView.CollectionViewLayout = new CustomTopCollectionViewLayout(topView.TopCollectionView.Frame);
        }


        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            tableView.RowHeight = UITableView.AutomaticDimension;
            tableView.ReloadData();
            collectionView.ScrollToItem(NSIndexPath.FromRowSection(0, 0), UICollectionViewScrollPosition.Left, true);
            UpdateNavigationBar();
        }
        #endregion

        #region Layouts
        private void AddConstraints()
        {
            View.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraint(NSLayoutConstraint.Create(topView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View, NSLayoutAttribute.TopMargin, 1.0f, 0f));
            View.AddConstraint(NSLayoutConstraint.Create(topView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, View, NSLayoutAttribute.Height, 0.2f, 0f));
            View.AddConstraint(NSLayoutConstraint.Create(topView, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, View, NSLayoutAttribute.Leading, 1.0f, 0f));
            View.AddConstraint(NSLayoutConstraint.Create(topView, NSLayoutAttribute.Trailing, NSLayoutRelation.Equal, View, NSLayoutAttribute.Trailing, 1.0f, 0f));
            View.TranslatesAutoresizingMaskIntoConstraints = true;

        }

        private void AddBindings()
        {
            var set = this.CreateBindingSet<FirstViewController, FirstViewModel>();
            set.Bind(collectionViewSource).For(v => v.ItemsSource).To(vm => vm.CollectionSource);
            set.Bind(tableViewSource).For(v => v.ItemsSource).To(vm => vm.TableSource);
            set.Bind(collectionViewSource).For(v => v.SelectionChangedCommand).To(vm => vm.SelectedCollectionItem);
            set.Bind(topCollectionViewSource).For(v => v.ItemsSource).To(vm => vm.TopCollectionSource);
            set.Bind(topCollectionViewSource).For(v => v.SelectionChangedCommand).To(vm => vm.SelectedTopCollectionItem);
            set.Apply();
        }
        #endregion

        #region Handlers
        private void AddSwipeGestureRecognizers()
        {
            var gestureRecognizer_Down = new UISwipeGestureRecognizer(SwipeDown);
            gestureRecognizer_Down.Direction = UISwipeGestureRecognizerDirection.Down;
            View.AddGestureRecognizer(gestureRecognizer_Down);

            var gestureRecognizer_Up = new UISwipeGestureRecognizer(SwipeUp);
            gestureRecognizer_Up.Direction = UISwipeGestureRecognizerDirection.Up;
            View.AddGestureRecognizer(gestureRecognizer_Up);
        }

        private void SwipeUp()
        {
            NavigationController.NavigationBarHidden = false;
            topView.Hidden = true;
            foreach (var constraint in View.Constraints)
            {
                if (constraint.FirstAttribute == NSLayoutAttribute.Top && constraint.SecondAttribute == NSLayoutAttribute.Bottom)
                {
                    TopCollectionView_TopConstraint = constraint;
                }
            }
            if (TopCollectionView_TopConstraint != null)
            {
                View.RemoveConstraint(TopCollectionView_TopConstraint);
                View.AddConstraint(NSLayoutConstraint.Create(collectionView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View, NSLayoutAttribute.Top, 1.0f, 0f));
            }
            View.LayoutIfNeeded();
        }

        private void SwipeDown()
        {
            NavigationController.NavigationBarHidden = true;
            foreach (var constraint in View.Constraints)
            {
                if (constraint.FirstAttribute == NSLayoutAttribute.Top && constraint.SecondAttribute == NSLayoutAttribute.Top)
                {
                    TopCollectionView_TopConstraint = constraint;
                }
            }
            if (TopCollectionView_TopConstraint != null)
            {
                View.RemoveConstraint(TopCollectionView_TopConstraint);              
                View.AddConstraint(NSLayoutConstraint.Create(collectionView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, topView, NSLayoutAttribute.Bottom, 1.0f, 0f));
            }                
            topView.Hidden = false;
            View.LayoutIfNeeded();
        }
        #endregion
    }
}