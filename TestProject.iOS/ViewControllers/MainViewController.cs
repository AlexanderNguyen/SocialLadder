﻿using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using System;
using TestProject.Core.ViewModels;
using UIKit;

namespace TestProject.iOS.ViewControllers
{
    [MvxRootPresentation]
    public partial class MainViewController : MvxTabBarViewController<MainViewModel>
    {
        //public MainViewController() : base("MainViewController", null)
        //{
        //}

        public MainViewController()
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            TabBar.BarTintColor = UIColor.White;
            TabBar.TintColor = UIColor.Blue;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            //UINavigationController.NavigationBar.Translucent = false;
            UINavigationBar.Appearance.TintColor = UIColor.FromRGB(232, 31, 138);
            //NavigationController.NavigationBar.BackgroundColor = UIColor.FromRGB(232, 31, 138);
            if (NavigationController != null)
            {
                NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(232, 31, 138);
            }


            //AppDelegate.NavController = this.NavigationController;
            ViewModel.ShowFirstView();
        }
    }
}