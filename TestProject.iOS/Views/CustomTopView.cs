﻿using Foundation;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Core.ViewModels;
using ObjCRuntime;
using System;
using UIKit;

namespace TestProject.iOS.Views
{
    public partial class CustomTopView : UIView
    {
        public UICollectionView TopCollectionView
        {
            get { return topCollectionView; }
        }

        public static CustomTopView Create()
        {
            var nsArr =  NSBundle.MainBundle.LoadNib("CustomTopView", null, null);
            var view = Runtime.GetNSObject<CustomTopView>(nsArr.ValueAt(0));
            return view;
        }

        public CustomTopView(IntPtr handle) : base (handle)
        {
        }
    }
}