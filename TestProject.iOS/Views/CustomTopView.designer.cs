﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestProject.iOS.Views
{
    [Register ("CustomTopView")]
    partial class CustomTopView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UICollectionView topCollectionView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (topCollectionView != null) {
                topCollectionView.Dispose ();
                topCollectionView = null;
            }
        }
    }
}