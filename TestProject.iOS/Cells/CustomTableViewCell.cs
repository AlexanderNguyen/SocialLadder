﻿using System;
using Core.Enums;
using Core.Models;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace TestProject.iOS.Cells
{
    public partial class CustomTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("tableViewCell");
        public static readonly UINib Nib;
        readonly UIColor purple = UIColor.FromRGB(186, 76, 217);
        readonly UIColor green = UIColor.FromRGB(76, 217, 132);
        readonly UIColor violet = UIColor.FromRGB(104, 76, 217);



        static CustomTableViewCell()
        {
            Nib = UINib.FromName("CustomTableViewCell", NSBundle.MainBundle);
        }

        protected CustomTableViewCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<CustomTableViewCell, CustomTableViewModel>();

                set.Bind(topText).For(lbl => lbl.Text).To(m => m.TopText);
                set.Bind(bottomText).For(lbl => lbl.Text).To(m => m.BottomText);
                //set.Bind(value).For(lbl => lbl.Text).To(m => m.Value);
                set.Apply();
            });
        }

        public void UpdateCell(CustomTableViewModel model)
        {
            leftImage.Image = UIImage.FromBundle(model.LeftImageName);
            rightImage.Image = UIImage.FromBundle(model.RightImageName);
            value.Text = model.Value + " Pts";
            if (model.State== CellState.First)
            {
                bottomView.BackgroundColor = leftView.BackgroundColor = purple;
                return;
            }
            if (model.State == CellState.Second)
            {
                bottomView.BackgroundColor = leftView.BackgroundColor = green;
                return;
            }
            if (model.State == CellState.Third)
            {
                bottomView.BackgroundColor = leftView.BackgroundColor = violet;
                return;
            }
        }
    }
}
