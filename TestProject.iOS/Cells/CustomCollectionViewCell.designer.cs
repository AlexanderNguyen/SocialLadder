﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestProject.iOS.Cells
{
    [Register ("CustomCollectionViewCell")]
    partial class CustomCollectionViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel bottomText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView image { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel progressText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIProgressView progressView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (bottomText != null) {
                bottomText.Dispose ();
                bottomText = null;
            }

            if (image != null) {
                image.Dispose ();
                image = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (progressText != null) {
                progressText.Dispose ();
                progressText = null;
            }

            if (progressView != null) {
                progressView.Dispose ();
                progressView = null;
            }
        }
    }
}