﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestProject.iOS.Cells
{
    [Register ("CustomTableViewCell")]
    partial class CustomTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel bottomText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView bottomView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView leftImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView leftView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView rightImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel topText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel value { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (bottomText != null) {
                bottomText.Dispose ();
                bottomText = null;
            }

            if (bottomView != null) {
                bottomView.Dispose ();
                bottomView = null;
            }

            if (leftImage != null) {
                leftImage.Dispose ();
                leftImage = null;
            }

            if (leftView != null) {
                leftView.Dispose ();
                leftView = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (rightImage != null) {
                rightImage.Dispose ();
                rightImage = null;
            }

            if (topText != null) {
                topText.Dispose ();
                topText = null;
            }

            if (value != null) {
                value.Dispose ();
                value = null;
            }
        }
    }
}