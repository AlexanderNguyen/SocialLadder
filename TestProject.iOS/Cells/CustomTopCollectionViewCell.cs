﻿using System;
using Core.Models;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace TestProject.iOS.Cells
{
    public partial class CustomTopCollectionViewCell : MvxCollectionViewCell
    {
        public static readonly NSString Key = new NSString("CustomTopCollectionViewCell");
        public static readonly UINib Nib;

        static CustomTopCollectionViewCell()
        {
            Nib = UINib.FromName("CustomTopCollectionViewCell", NSBundle.MainBundle);
        }

        protected CustomTopCollectionViewCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<CustomTopCollectionViewCell, CustomTopCollectionViewModel>();
                set.Bind(Title).For(lbl => lbl.Text).To(m => m.Title);
                set.Bind(HalfValue).For(lbl => lbl.Text).To(m => m.HalfValue);
                set.Bind(HalfValue).For(lbl => lbl.Hidden).To(m => m.HaveSubview).WithConversion("VisibilityConverter");
                set.Bind(HalfImage).For(img => img.Hidden).To(m => m.HaveSubview).WithConversion("VisibilityConverter");
                set.Bind(SubImage).For(img => img.Hidden).To(m => m.HaveSubview).WithConversion("VisibilityConverter");
                set.Apply();
            });
        }

        public void UpdateCell(CustomTopCollectionViewModel model)
        {
            MainImage.Image = UIImage.FromBundle(model.BckgroundImageName);
            if (model.HaveSubview)
            {
                SubImage.Image = UIImage.FromBundle(model.SubViewImageName);
                HalfImage.Image = UIImage.FromBundle(model.HalfImageName);
            }
        }


    }
}