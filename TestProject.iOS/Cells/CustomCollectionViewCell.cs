﻿using System;
using System.Collections.Generic;
using Core.Models;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace TestProject.iOS.Cells
{
    public partial class CustomCollectionViewCell : MvxCollectionViewCell
    {
        public static readonly NSString Key = new NSString("collectionViewCell");
        public static readonly UINib Nib;

        static CustomCollectionViewCell()
        {
            Nib = UINib.FromName("CustomCollectionViewCell", NSBundle.MainBundle);
        }

        protected CustomCollectionViewCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<CustomCollectionViewCell, CustomCollectionViewModel>();
                set.Bind(progressView).For(lbl => lbl.Progress).To(m => m.ValueProgress);
                set.Bind(bottomText).For(lbl => lbl.Text).To(m => m.BottomText);
                set.Bind(mainView).For(lbl => lbl.BackgroundColor).To(m => m.BackgroundColor).WithConversion("ColorConverter");
                set.Apply();
            });
        }

        public void UpdateCell(CustomCollectionViewModel model)
        {
            progressText.Text = model.ValueProgress + " of " + model.MaxProgress;
            image.Image = UIImage.FromBundle(model.ImageName);
            if (model.Selected)
            {
                UpdateItem(1.0f);
            }
            if (!model.Selected)
            {
                UpdateItem(0.9f);
            }
        }

        public void UpdateItem(nfloat constraintSize)
        {
            mainView.TranslatesAutoresizingMaskIntoConstraints = false;
            List<NSLayoutConstraint> constants = new List<NSLayoutConstraint>();
            foreach (var constraint in ContentView.Constraints)
            {
                if (constraint.FirstAttribute == NSLayoutAttribute.Width || constraint.FirstAttribute == NSLayoutAttribute.Height || constraint.FirstAttribute == NSLayoutAttribute.CenterX || constraint.FirstAttribute == NSLayoutAttribute.CenterY)
                {
                    constants.Add(constraint);
                }
            }
            ContentView.RemoveConstraints(constants.ToArray());
            ContentView.AddConstraint(NSLayoutConstraint.Create(mainView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.CenterX, 1.0f, 0f));
            ContentView.AddConstraint(NSLayoutConstraint.Create(mainView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.CenterY, 1.0f, 0f));
            ContentView.AddConstraint(NSLayoutConstraint.Create(mainView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Height, constraintSize, 0f));
            ContentView.AddConstraint(NSLayoutConstraint.Create(mainView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ContentView, NSLayoutAttribute.Width, constraintSize, 0f));      
        }
    }
}