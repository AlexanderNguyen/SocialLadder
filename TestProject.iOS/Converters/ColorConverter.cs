﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Foundation;
using MvvmCross.Platform.Converters;
using MvvmCross.Platform.UI;
using UIKit;

namespace TestProject.iOS.Converters
{
    public class ColorConverter : MvxValueConverter<MvxColor, UIColor>
    {
        protected override UIColor Convert(MvxColor value, Type targetType, object parameter, CultureInfo culture)
        {
            return UIColor.FromRGB(value.R, value.G, value.B);
        }
    }
}