﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Models;
using Foundation;
using MvvmCross.Binding.iOS.Views;
using TestProject.iOS.Cells;
using UIKit;

namespace TestProject.iOS.Sources
{
    public class CustomCollectionViewSource : MvxCollectionViewSource
    {
        public CustomCollectionViewSource(UICollectionView collectionView) : base(collectionView)
        {
            collectionView.RegisterNibForCell(CustomCollectionViewCell.Nib, CustomCollectionViewCell.Key);
        }

        protected override UICollectionViewCell GetOrCreateCellFor(UICollectionView collectionView, NSIndexPath indexPath, object item)
        {
            //base.GetOrCreateCellFor(collectionView, indexPath, item);
            try
            {
                var model = (item as CustomCollectionViewModel);
                var cell = collectionView.DequeueReusableCell(CustomCollectionViewCell.Key, indexPath) as CustomCollectionViewCell;
                if (cell == null)
                {
                    return new MvxCollectionViewCell();
                }

                cell.UpdateCell(model);
                return cell;
            }
            catch (Exception ex)
            {
                return new MvxCollectionViewCell();
            }
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            base.ItemSelected(collectionView, indexPath);
            collectionView.ReloadData();
        }
    }
}