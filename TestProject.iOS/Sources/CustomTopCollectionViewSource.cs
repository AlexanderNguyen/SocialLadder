﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Models;
using Foundation;
using MvvmCross.Binding.iOS.Views;
using TestProject.iOS.Cells;
using UIKit;

namespace TestProject.iOS.Sources
{
    public class CustomTopCollectionViewSource : MvxCollectionViewSource
    {
        public CustomTopCollectionViewSource(UICollectionView collectionView) : base(collectionView)
        {
            collectionView.RegisterNibForCell(CustomTopCollectionViewCell.Nib, CustomTopCollectionViewCell.Key);
        }

        protected override UICollectionViewCell GetOrCreateCellFor(UICollectionView collectionView, NSIndexPath indexPath, object item)
        {
            try
            {
                var model = (item as CustomTopCollectionViewModel);
                var cell = collectionView.DequeueReusableCell(CustomTopCollectionViewCell.Key, indexPath) as CustomTopCollectionViewCell;
                if (cell == null)
                {
                    return new MvxCollectionViewCell();
                }

                cell.UpdateCell(model);
                return cell;
            }
            catch (Exception ex)
            {
                return new MvxCollectionViewCell();
            }
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            base.ItemSelected(collectionView, indexPath);
            collectionView.ReloadData();
        }
    }
}