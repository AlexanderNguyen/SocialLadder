﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Models;
using Foundation;
using MvvmCross.Binding.iOS.Views;
using TestProject.iOS.Cells;
using UIKit;

namespace TestProject.iOS.Sources
{
    public class CustomTableViewSource : MvxTableViewSource
    {
        private readonly string[] titleForSections = new string[] {"First section","Second section", "Third section" , "Fourth section" };
        private nint numberOfSection = 4;
        private nint rowInSection = 1;
        public CustomTableViewSource(UITableView tableView) : base(tableView)
        {
            tableView.RegisterNibForCellReuse(CustomTableViewCell.Nib, CustomTableViewCell.Key);
        }
        
        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            try
            {
                var model = (item as CustomTableViewModel);
                var cell = tableView.DequeueReusableCell(CustomTableViewCell.Key, indexPath) as CustomTableViewCell;
                if (cell == null)
                {
                    return new MvxTableViewCell();
                }

                cell.UpdateCell(model);
                return cell;
            }
            catch (Exception ex)
            {
                return new MvxTableViewCell();
            }

        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            base.RowSelected(tableView, indexPath);
            tableView.DeselectRow(indexPath, false);
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return UITableView.AutomaticDimension;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return numberOfSection;
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            return titleForSections[section];
        }

    }
}