﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Interfaces;
using Foundation;
using UIKit;

namespace TestProject.iOS.Platform
{
    public class AlertService : IAlertService
    {
        private readonly int duration = 2;

        public void ShowToast(string message)
        {
            UIApplication.SharedApplication.InvokeOnMainThread(() =>
            {
                UIAlertView alert = new UIAlertView()
                {
                    Message = message,
                    Alpha = 1.0f
                };
                NSTimer tmr;
                alert.Show();

                tmr = NSTimer.CreateTimer(duration, delegate
                {
                    alert.DismissWithClickedButtonIndex(0, true);
                    alert = null;
                });
                NSRunLoop.Main.AddTimer(tmr, NSRunLoopMode.Common);
            });
        }
    }
}