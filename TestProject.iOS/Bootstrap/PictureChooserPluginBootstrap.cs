using MvvmCross.Platform.Plugins;

namespace TestProject.iOS.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.PictureChooser.PluginLoader, MvvmCross.Plugins.PictureChooser.iOS.Plugin>
    {
    }
}