﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;

namespace TestProject.iOS.Layouts
{
    public class CustomCollectionViewLayout : UICollectionViewFlowLayout
    {
        public CustomCollectionViewLayout(CGRect frame)
        {
            ScrollDirection = UICollectionViewScrollDirection.Horizontal;
            ItemSize = new CGSize((frame.Height * 0.9f), (frame.Height * 0.9f));
            //SectionInset = new UIEdgeInsets(frame.Height * 0.1f, frame.Height * 0.1f, frame.Height * 0.1f, frame.Height * 0.1f);
            SectionInset = new UIEdgeInsets(0, frame.Height * 0.1f, 0, frame.Height * 0.1f);
        }

        public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds)
        {
            return true;
        }
    }
}