﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Binding.Droid.Target;
using MvvmCross.Platform.Exceptions;
using MvvmCross.Platform.Platform;

namespace TestProject.Droid.CustonBindings
{
    public class TextColorBinding : MvxAndroidTargetBinding
    {

        private readonly TextView _textView;

        public TextColorBinding(TextView textView) : base(textView)
        {

            _textView = textView;
        }

        public static void Register(IMvxTargetBindingFactoryRegistry registry)
        {
            registry.RegisterFactory(new MvxCustomBindingFactory<TextView>("TextColor", (textView) => new TextColorBinding(textView)));
        }

        #region implemented abstract members of MvxTargetBinding

        public override Type TargetType
        {
            get { return typeof(Color); }
        }

        #endregion

        #region implemented abstract members of MvxConvertingTargetBinding

        protected override void SetValueImpl(object target, object value)
        {
            var color = (Color)value;

            if (color != null)
            {

                var textView = (TextView)target;

                try
                {
                    textView.SetTextColor(color);
                }
                catch (Exception ex)
                {
                    MvxTrace.Error(ex.ToLongString());
                    throw;
                }

            }
            else
            {
                MvxBindingTrace.Trace(MvxTraceLevel.Warning, "Value was not a valid Color");
            }
        }
        #endregion
    }
}