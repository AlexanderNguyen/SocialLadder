using MvvmCross.Platform.Plugins;

namespace TestProject.Droid.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.PictureChooser.PluginLoader>
    {
    }
}