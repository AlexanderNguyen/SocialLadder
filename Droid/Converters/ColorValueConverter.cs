﻿using System;
using System.Globalization;
using Android.Graphics;
using Android.Graphics.Drawables;
using MvvmCross.Platform.Converters;
using MvvmCross.Platform.UI;

namespace TestProject.Droid.Converters
{
    public class ColorValueConverter : MvxValueConverter<MvxColor, Color>
    {
        protected override Color Convert(MvxColor value, Type targetType, object parameter, CultureInfo culture)
        {
            var color1 = value.ToString();
            Color color = Color.Rgb(value.R, value.G, value.B);
            return color;
        }
    }
}