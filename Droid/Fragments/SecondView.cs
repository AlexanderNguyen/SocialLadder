﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Core.ViewModels;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views.Attributes;
using TestProject.Core.ViewModels;

namespace TestProject.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("testProject.droid.fragments.SecondView")]
    public class SecondView : BaseFragment<SecondViewModel>
    {
        protected override int FragmentId => Resource.Layout.SecondViewLayout;
        public MvxDatePicker picker;
        private ScrollView scrollView;
        //private HorizontalScrollView horScrollView;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            var text = view.FindViewById<EditText>(Resource.Id.text);
            var font = Typeface.CreateFromAsset(Activity.Assets, "ArialRegular.ttf");

            picker = view.FindViewById<MvxDatePicker>(Resource.Id.picker);
            text.Typeface = font;

           //horScrollView.PageScroll( FocusSearchDirection.Left );

            return view;
        }
    }
}