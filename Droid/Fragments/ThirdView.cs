﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Core.ViewModels;
using MvvmCross.Droid.Views.Attributes;
using TestProject.Core.ViewModels;
using static Android.Views.View;

namespace TestProject.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("testProject.droid.fragments.ThirdView")]
    public class ThirdView : BaseFragment<ThirdViewModel>, IOnScrollChangeListener
    {
        protected override int FragmentId => Resource.Layout.ThirdViewLayout;
        private HorizontalScrollView scrollView;
        private LinearLayout first, second, third;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            scrollView = view.FindViewById<HorizontalScrollView>(Resource.Id.scroll);
            first = view.FindViewById<LinearLayout>(Resource.Id.firstL);
            second = view.FindViewById<LinearLayout>(Resource.Id.secondL);
            third = view.FindViewById<LinearLayout>(Resource.Id.thirdL);
            scrollView.HorizontalScrollBarEnabled = true;
            scrollView.SetOnScrollChangeListener(this);
            
            return view;
        }

        public override void OnResume()
        {
            base.OnResume();
            //scrollView.MaxScrollAmount = first.LayoutParameters.Width;
            scrollView.SmoothScrollingEnabled = true;
            //scrollView.PageScroll(FocusSearchDirection.Right);
        }

        public int counter = 0;

        public void OnScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY)
        {
            if (scrollX > oldScrollX && (scrollX - oldScrollX) <= (second.LayoutParameters.Width / Resources.DisplayMetrics.Density))
            {
                //scrollView.ScrollTo(second.LayoutParameters.Width, 0);
                //scrollView.FullScroll(FocusSearchDirection.Right);
                scrollView.SmoothScrollBy(scrollX, 0);
                scrollView.SmoothScrollTo((int)(second.LayoutParameters.Width / Resources.DisplayMetrics.Density), 0);
                return;
            }
            if (scrollX < oldScrollX)
            {
                //scrollView.ScrollTo(0, 0);
                //scrollView.FullScroll(FocusSearchDirection.Left);
                scrollView.SmoothScrollBy(scrollX, 0);
                scrollView.SmoothScrollTo(0,0);
                return;
            }
        }
    }
}