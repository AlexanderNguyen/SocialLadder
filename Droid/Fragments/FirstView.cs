﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Core.ViewModels;
using MvvmCross.Binding.Droid.BindingContext;
using TestProject.Core.ViewModels;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views.Attributes;
using MvvmCross.Binding.Droid.Views;
using TestProject.Droid.Adapters;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platform;
using MvvmCross.Plugins.PictureChooser;
using com.cunoraz.tagview;
using Android.Graphics;
using Android.Media;
using Android.Provider;
using Android.Database;

namespace TestProject.Droid.Fragments
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("testProject.droid.fragments.FirstView")]
    public class FirstView : BaseFragment<FirstViewModel>, TagView.IOnTagClickListener, TagView.IOnTagDeleteListener, TagView.IOnTagLongClickListener
    {
        protected override int FragmentId => Resource.Layout.FirstViewLayout;
        TagView tagGroup;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {      
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            var pathUri = Android.Net.Uri.Parse("android.resource://" + this.Context.PackageName + "/" + Resource.Drawable.video);
            VideoView videoView = view.FindViewById<VideoView>(Resource.Id.SampleVideoView);
            videoView.SetVideoURI(pathUri);
            videoView.Start();

            //var task = Mvx.Resolve<IMvxPictureChooserTask>();

            //var firstList = view.FindViewById<MvxListView>(Resource.Id.first_list);
            //firstList.Adapter = new FirstListAdapter(Activity, (IMvxAndroidBindingContext)BindingContext);

            //var set = this.CreateBindingSet<FirstView, FirstViewModel>();
            //set.Bind(firstList).For(c => c.ItemClick).To(vm => vm.SelectedCommand);
            //set.Apply();

            tagGroup = (TagView)view.FindViewById(Resource.Id.tag_group);
            var tag = new Tag("test1");
            tag.isDeletable = true;
            tag.layoutBorderColor = Resource.Color.black;
            tag.layoutBorderSize = 5;
            tag.layoutColor = Resource.Color.material_blue_grey_800;
            tagGroup.AddTag(tag);
            tagGroup.AddTag(tag);
            tagGroup.AddTag(new Tag("testtestets3"));
            tagGroup.AddTag(new Tag("testtestssafgasdfets3"));
            tagGroup.AddTag(new Tag("testtesasdfsadfsadfasdfdsatets3"));
            tagGroup.AddTag(new Tag("testteasdfsadstets3"));
            tagGroup.AddTag(tag);
            tagGroup.AddTag(new Tag("testtsdafasdfasdfsadfasfsadfasdfsadfasdfdsafestets3"));
            //tagGroup.AddTag(List < Tag > tags);
            //AddTags(string[] tags);

            tagGroup.SetOnTagClickListener(this);
            tagGroup.SetOnTagDeleteListener(this);
            tagGroup.SetOnTagLongClickListener(this);

            return view;
        }

        public void OnTagClick(Tag tag, int position)
        {

            MediaPlayer mp;
            var pathToNewPhoto = Android.Net.Uri.Parse("android.resource://TestProject/" + Resource.Drawable.splash);
            //var imageUri = Android.Net.Uri.FromFile(new Java.IO.File(pathToNewPhoto));

        }

        public void OnTagDeleted(TagView view, Tag tag, int position)
        {
            tagGroup.Remove(position);
        }

        public void OnTagLongClick(Tag tag, int position)
        {

        }
    }
}