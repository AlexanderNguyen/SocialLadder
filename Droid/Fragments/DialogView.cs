﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Core.ViewModels;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Views.Attributes;
using TestProject.Core.ViewModels;

namespace TestProject.Droid.Fragments
{
    [MvxDialogFragmentPresentation]
    [Register(nameof(DialogView))]
    public class DialogView : MvxDialogFragment<DialogViewModel>
    {
        public DialogView()
        {
        }

        protected DialogView(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignore = base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(Resource.Layout.DialogViewLayout, null);
            //SetStyle(0, Resource.Style.FullScreen);
            try
            {
                //var par = view.LayoutParameters;
                //par.Width = Resources.DisplayMetrics.WidthPixels;
                //par.Height = Resources.DisplayMetrics.HeightPixels;
                //view.LayoutParameters = par;
                //view.Invalidate();

                // this.Dialog.Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
                Dialog.Window.SetBackgroundDrawableResource(Android.Resource.Color.Transparent);


                var dialog = view.FindViewById<LinearLayout>(Resource.Id.dialog);
                var par = dialog.LayoutParameters;
                par.Width = Resources.DisplayMetrics.WidthPixels;
                par.Height = Resources.DisplayMetrics.HeightPixels;
                dialog.LayoutParameters = par;
                dialog.Invalidate();

            }
            catch (Exception ex)
            {
            }
            return view;
        }
    }
}