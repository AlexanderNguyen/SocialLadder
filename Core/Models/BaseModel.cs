﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class BaseModel
    {
        private String id;
        public String Id
        {
            get => id ?? (id = Guid.NewGuid().ToString());
            set => id = value;
        }
    }
}
