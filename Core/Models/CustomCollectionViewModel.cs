﻿using MvvmCross.Platform.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CustomCollectionViewModel : BaseModel
    {
        public string ImageName { get; set; }
        public int MinProgress { get; set; }
        public float MaxProgress { get; set; }
        public float ValueProgress { get; set; }
        public string ProgressText { get; set; }
        public string BottomText { get; set; }
        public bool Selected { get; set; }
        public MvxColor BackgroundColor { get; set; }
    }
}
