﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CustomTableViewModel : BaseModel
    {
        public string LeftImageName { get; set; }
        public string RightImageName { get; set; }
        public int Value { get; set; }
        public string BottomText { get; set; }
        public string TopText { get; set; }
        public CellState State { get; set; }
    }
}
