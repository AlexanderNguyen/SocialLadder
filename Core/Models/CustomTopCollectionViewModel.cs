﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class CustomTopCollectionViewModel : BaseModel
    {
        public string BckgroundImageName { get; set; }
        public string SubViewImageName { get; set; }
        public string HalfImageName { get; set; }
        public float ValueProgress { get; set; }
        public string Title { get; set; }
        public bool Selected { get; set; }
        public bool HaveSubview { get; set; }
        public int HalfValue { get; set; }
        public bool IsLastItem { get; set; }
    }
}
