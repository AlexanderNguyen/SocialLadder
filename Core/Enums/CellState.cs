﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Enums
{
    public enum CellState
    {
        None = 0,
        First = 1,
        Second = 2,
        Third = 3,
        Fourth = 4
    }
}
