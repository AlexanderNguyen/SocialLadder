using MvvmCross.Platform.Plugins;

namespace Core.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.PictureChooser.PluginLoader>
    {
    }
}