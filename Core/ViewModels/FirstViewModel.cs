﻿using Core.Interfaces;
using Core.Models;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.PictureChooser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Core.ViewModels
{
    public class FirstViewModel : BaseViewModel
    {
        #region Fields and Properties
        private readonly IMvxNavigationService _mvxNavigationService;
        private readonly IAlertService _alertService;

        private MvxObservableCollection<CustomCollectionViewModel> _collectionSource;
        public MvxObservableCollection<CustomCollectionViewModel> CollectionSource
        {
            get { return _collectionSource; }
            set { _collectionSource = value; RaisePropertyChanged("CollectionSource"); }
        }

        private MvxObservableCollection<CustomTableViewModel> _tableSource;
        public MvxObservableCollection<CustomTableViewModel> TableSource
        {
            get { return _tableSource; }
            set { _tableSource = value; RaisePropertyChanged("TableSource"); }
        }

        private MvxObservableCollection<CustomTopCollectionViewModel> _topCollectionSource;
        public MvxObservableCollection<CustomTopCollectionViewModel> TopCollectionSource
        {
            get { return _topCollectionSource; }
            set { _topCollectionSource = value; RaisePropertyChanged("TopCollectionView"); }
        }
        #endregion

        public FirstViewModel(IMvxNavigationService mvxNavigationService, IAlertService alertService)
        {
            _alertService = alertService;
            _mvxNavigationService = mvxNavigationService;
            var result = Plugin.SecureStorage.CrossSecureStorage.Current.HasKey("test");
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            #region HardCode
            CollectionSource = new MvxObservableCollection<CustomCollectionViewModel>();
            CollectionSource.Add(new CustomCollectionViewModel { BottomText = "Bottom text 1", Id = "1", ImageName = "ic_bullhorn", MaxProgress = 0.9f, MinProgress = 0, ValueProgress = 0.55f, Selected = false, BackgroundColor = new MvxColor(255, 42, 0) });
            CollectionSource.Add(new CustomCollectionViewModel { BottomText = "Bottom text 2 Bottom text 2 Bottom text 2", Id = "2", ImageName = "ic_bullhorn", MaxProgress = 0.7f, MinProgress = 3, ValueProgress = 0.33f, Selected = false, BackgroundColor = new MvxColor(55, 242, 0) });
            CollectionSource.Add(new CustomCollectionViewModel { BottomText = "Bottom text 3 Bottom text 3 Bottom text 3 Bottom text 3 Bottom text 3", Id = "3", ImageName = "ic_bullhorn", MaxProgress = 0.5f, MinProgress = 98, ValueProgress = 0.11f, Selected = false, BackgroundColor = new MvxColor(15, 42, 211) });

            TableSource = new MvxObservableCollection<CustomTableViewModel>();
            TableSource.Add(new CustomTableViewModel { BottomText = "Bottom text 1", LeftImageName = "ic_photo", RightImageName = "ic_points", TopText = "Top text 1", Value = 100 });
            TableSource.Add(new CustomTableViewModel { BottomText = "Bottom text 2 Bottom text 2 Bottom text 2 Bottom text 2", LeftImageName = "ic_photo", RightImageName = "ic_points",  TopText = "Top text 2 Top text 2 Top text 2 Top text 2", Value = 45 });
            TableSource.Add(new CustomTableViewModel { BottomText = "Bottom text 3 Bottom text 3 Bottom text 3 Bottom text 3 Bottom text 3 Bottom text 3", LeftImageName = "ic_photo", RightImageName = "ic_points", TopText = "Top text 3 Top text 3 Top text 3 Top text 3 Top text 3", Value = 112 });
            TableSource.Add(new CustomTableViewModel { BottomText = "Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4 Bottom text 4", LeftImageName = "ic_photo", RightImageName = "ic_points", TopText = "Top text 4 Top text 4 Top text 4 Top text 4 Top text 4", Value = 98 });

            TopCollectionSource = new MvxObservableCollection<CustomTopCollectionViewModel>();
            TopCollectionSource.Add(new CustomTopCollectionViewModel { BckgroundImageName = "TopBackgroundImage1", Title = "SocialLadder", ValueProgress = 50, HaveSubview = true, HalfValue = 365, SubViewImageName = "WhiteCitcle", HalfImageName = "HalfCircle", IsLastItem = false });
            TopCollectionSource.Add(new CustomTopCollectionViewModel { BckgroundImageName = "TopBackgroundImage2", Title = "SocialLadder", ValueProgress = 50, HaveSubview = false, IsLastItem = false });
            AddLastTopCollectionViewItem();
            #endregion
        }

        #region Commands
        public MvxCommand<CustomCollectionViewModel> SelectedCollectionItem
        {
            get
            {
                return new MvxCommand<CustomCollectionViewModel>((param) =>
                {
                    var collection = CollectionSource;
                    collection.Where(x => x.Id != param.Id).ToList().ForEach(c => c.Selected = false);
                    var selectedItem = collection.Where(x => x.Id == param.Id).FirstOrDefault();
                    selectedItem.Selected = selectedItem.Selected == true ? false : true;
                    CollectionSource = collection;
                    _alertService.ShowToast("Click command");
                });
            }
        }

        public MvxCommand<CustomTopCollectionViewModel> SelectedTopCollectionItem
        {
            get
            {
                return new MvxCommand<CustomTopCollectionViewModel>((param) =>
                {
                    if (param.IsLastItem)
                    {
                        var newItem = new CustomTopCollectionViewModel { BckgroundImageName = "TopBackgroundImage2", Title = "SocialLadder", ValueProgress = 50, HaveSubview = false, IsLastItem = false };
                        TopCollectionSource.Insert(TopCollectionSource.Count - 1, newItem);
                        _alertService.ShowToast("Add command");
                        return;
                    }
                    _alertService.ShowToast("Click command");
                });
            }
        }
        #endregion

        #region Methods
        private void AddLastTopCollectionViewItem()
        {
            TopCollectionSource.Add(new CustomTopCollectionViewModel { BckgroundImageName = "TopBackgroundImage3", Title = "SocialLadder", ValueProgress = 50, HaveSubview = false, IsLastItem = true });
        }
        #endregion
    }
}
