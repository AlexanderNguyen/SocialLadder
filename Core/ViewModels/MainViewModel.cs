﻿using Core.ViewModels;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Core.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        public MainViewModel(IMvxNavigationService mvxNavigationService)
        {
            _navigationService = mvxNavigationService;
        }

        public async void ShowFirstView()
        {
            await _navigationService.Navigate<FirstViewModel>();
        }
    }
}
